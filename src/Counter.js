import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Counter extends Component {
  constructor(props){
      super(props);//调用父类React.Component的构造函数；
      this.onClickIncrementButton= this.onClickIncrementButton.bind(this);
      this.onClickDecrementButton = this.onClickDecrementButton.bind(this);
      this.state = {//组件的内部状态
          count:props.initValue || 0
      }
  }
  onClickIncrementButton(){
      this.setState({
          count:this.state.count + 1
      })
  }
  onClickDecrementButton(){
    this.setState({
        count:this.state.count - 1
    })
}
  render(){
      const {caption} = this.props;
      const buttonStyle = {
          border:'1px solid #000',
          height:'30px',
          width:"100px",
          display:'inline-block'
      }
      return(
          <div>
              <button style={buttonStyle} onClick = {this.onClickIncrementButton}>+</button>
              <span>{caption} count:{this.state.count}</span>
              <button style={buttonStyle} onClick = {this.onClickDecrementButton}>-</button>
              
          </div>
      )
  }
}

export default Counter;

// 装载过程 Mount  组件第一次在Dom树中渲染的过程
// 更新过程 Update 组件被重新渲染的过程
// 卸载过程 Unmount 组件从dom中删除的过程
